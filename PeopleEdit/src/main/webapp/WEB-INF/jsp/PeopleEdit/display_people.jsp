
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.People.People"%>
<%@page import="com.People.PeopleDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />




<portlet:renderURL var="GoHome">
	<portlet:param name="action" value="HomePage" />
</portlet:renderURL>
<a href="<%=GoHome%>">Home</a>


<portlet:resourceURL id="GetPeopleList" var="GetPeopleList"></portlet:resourceURL>
<div>
	<h3 align="center">List of Peoples</h3>
	<table align="Center" border="1" cellpadding="30%"
		id="<portlet:namespace/>people_list">
		<thead>
			<tr>
				<th>FullName</th>
				<th>DOBString</th>
				<th>Gender</th>
				<th>PhNo</th>
				<th>Addr1</th>
			</tr>
		</thead>
		<tbody id='tbody'>

		</tbody>
	</table>

	<input type="text" id="Fname" />
</div>
<script type="text/javascript">
$(document).ready(function() {
	
	$("#<portlet:namespace/>people_list").dataTable({
		"responsive": true,
		"pageLength":5,
	 	"pagingType" : "simple",
		"paging":true,
		"ajax": {
		    "url": "<%=GetPeopleList%>",
		    "type": "POST",
		    "data": function (data) {
			  return $.extend( {}, data, {
			  });
			} 
		 },
		"columnDefs": [
			{
				"targets": [ 0 ],
			    "data": "FullName",
			    "width": "20%",
			    "orderable":false,
			     "render": function (data, type, row) {
			       		 return row.FullName;
			    	}
			},
			{
				"targets":[1],
				"data" : "DOBString",
				"width" : "20%",
				"orderable": false,
				"render": function(data, type,row){
					return row.DOBString;
				}
			},
				{
					"targets":[2],
					"data" : "Gender",
					"width" : "20%",
					"orderable": false,
					"render": function(data, type,row){
						return row.Gender;
					}
				},
				{
						"targets":[3],
						"data" : "PhNo",
						"width" : "20%",
						"orderable": false,
						"render": function(data, type,row){
							return row.PhNo;
						}
				},
					{
						"targets":[4],
						"data" : "Addr1",
						"width" : "20%",
						"orderable": false,
						"render": function(data, type,row){
							return row.Addr1;
						}
					}
		]
	});
}
</script>