<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects/>


<portlet:actionURL var="submitAction" name="insertPeople"/>

<!DOCTYPE html>
<html>
<head>

 <style>
 tr {
   height:25px;
 }
</style>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form modelAttribute="people" method="post" name="peopleForm" action="<%=submitAction.toString() %>">

<table style="width: 100%">

<tr>
<td style="width=13%">Honorific:</td><td style="width=20%"> <input type="text" name="honorific"/></td>
<td style="width=13%" >Suffix:</td><td style="width=20%"> <input type="text" name="suffix" /></td>
<td style="width=13%" >Alias:</td><td style="width=20%"> <input type="text" name="alias" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">First Name:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />fName" /></td>
<td style="width=13%">Middle Name:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />mName" /></td>
<td style="width=14%">Last Name:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />lName" /></td>
</tr>
<tr/>

<tr>
<td style="width=13%">Addr1:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />addr1" /></td>
<td style="width=13%">Addr2:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />addr2" /></td>
<td style="width=14%">City:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />city" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">State:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />state" /></td>
<td style="width=13%">Country:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />country" /></td>
<td style="width=14%">Zipcode:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />zipcode" /></td>
</tr>


<tr/>

<tr>
<td style="width=13%">Gender:</td><td style="width=20%"> <input type="radio" id="GenderChoice1" name="<portlet:namespace />gender" value="M"/> <label for="GenderChoice1">Male</label>
<input type="radio" id="GenderChoice2" name="<portlet:namespace />gender" value="F" /> <label for="GenderChoice2">Female</label>
</td>
<td style="width=13%">SSN:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />sSN" /></td>
<td style="width=14%">Date of Birth:</td><td style="width=20%"><label for="DOBdate"/> <input type="date" id="DOBdate" name="<portlet:namespace />dOB" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">Email:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />email" /></td>
<td style="width=13%">Phone Number:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />phNo" /></td>
<td style="width=14%">Alternate Ph Number:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />altPhNo" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">Driver's License Number:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />dLN" /></td>
<td style="width=13%">Driver's License State:</td><td style="width=20%"> <input type="text" name="<portlet:namespace /> dLNLicSt" /></td>
<td style="width=14%">Driver's License Type:</td><td style="width=20%"> <select id="DLNLicType" name="<portlet:namespace />dLNLicType">
<option value=" "> </option>
<option value="Operator">Operator</option>
<option value="Chauffeur">Chauffeur</option>
<option value="Motorcycle">Motorcycle</option>
<option value="Limited-term">Limited-term</option>
</select>
</td>
</tr>
<tr/>
<tr>
<td style="width=13%">Primary Language:</td><td style="width=20%"> <select name="<portlet:namespace />primLang" id="Primlang">
<option value="English">English</option>
<option value="Spanish">Spanish</option>
<option value="Arabic">Arabic</option>
<option value="Unknown">Unknown</option>
</select></td>
<td style="width=13%">Ethnicity:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />ethnicity" /></td>
<td style="width=14%">Race:</td><td style="width=20%" colspan=3> <input type="text" name="<portlet:namespace />race" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">Interpreter Required:</td><td style="width=20%"> <input type="radio" id="IntPrtReqYes" value="Y" name="<portlet:namespace />intrptrReq" /> <label for="IntPrtReqYes">Yes</label>
<input type="radio" name="<portlet:namespace />intrptrReq" id="IntPrtReqNo" value="N"/><label for="IntPrtReqNo">No</label>
</td>
<td style="width=13%">LEP:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />lEP" data-tip="Limited English Proficiency" /></td>
<td style="width=14%">PPO:</td><td style="width=20%"> <input type="text" name="<portlet:namespace />pPO" data-tip="Preferred Provider Organization" /></td>
</tr>
<tr/>
<tr>
<td style="width=13%">Deceased:</td><td style="width=20%" colspan=3> <input type="radio" id="DeceasedYes" value="Y" name="<portlet:namespace />deceased" /> <label for="DeceasedYes">Yes</label>
<input type="radio" id="DeceasedNo" value="N" name="<portlet:namespace />deceased"/><label for="DeceasedNo">No</label></td>
</tr>
<tr/>
<tr>
<td colspan=5 style="width=20%"><input type="Submit" value="Submit" /></td>
</tr>
</table>

</form:form>

</body>
</html>