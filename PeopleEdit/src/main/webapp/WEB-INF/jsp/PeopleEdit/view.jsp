<%@ include file="init.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="javax.portlet.PortletURL"%>
 
<%@page import="com.liferay.taglib.portlet.ActionURLTag"%>
<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import="com.People.People" %>
<%@page import="java.util.List" %>
<portlet:defineObjects />

<portlet:renderURL var="addPeopleURL">
	<portlet:param name="action" value="addPeople"/>
</portlet:renderURL>

<a href="<%=addPeopleURL %>">Add People</a>

<portlet:renderURL var="DisplayRenderURL">
<portlet:param name="action" value="displayForm"/>
</portlet:renderURL>

<a href="<%=DisplayRenderURL %>">Display People</a>

<portlet:renderURL var="updatePeopleURL">
	<portlet:param name="action" value="updatePeople"/>
</portlet:renderURL>

<a href="<%=updatePeopleURL %>">Update People</a>

<portlet:renderURL var="searchPeopleURL">
	<portlet:param name="action" value="searchPeople"/>
</portlet:renderURL>

<a href="<%=searchPeopleURL %>">Search People</a>

<portlet:renderURL var="searchPeopleNameURL">
	<portlet:param name="action" value="searchPeopleName"/>
</portlet:renderURL>

<a href="<%=searchPeopleNameURL.toString() %>">Search People By Name</a>

<portlet:renderURL var="updatePeopleIDURL">
	<portlet:param name="action" value="updatePeopleID"/>
</portlet:renderURL>




