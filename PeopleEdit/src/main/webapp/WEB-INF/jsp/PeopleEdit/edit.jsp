<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects/>

<portlet:actionURL name="performUpdate" var="performUpdate">
</portlet:actionURL>

<%
 String name = renderRequest.getParameter("fullname");
 String age = renderRequest.getParameter("age");
 String gender = renderRequest.getParameter("gender");
 String ssn = renderRequest.getParameter("ssn");
 String email = renderRequest.getParameter("email");
%>

<h3>Edit Student: <%= name%></h3>

<aui:form action="<%=performUpdate%>" method="post" name="name">
 <aui:input name="name" type="text" value="<%=name%>"/>
 <aui:input name="gender" type="text" value="<%=gender %>"/>
 <aui:input name="age" type="number" value="<%=Integer.parseInt(age) %>"/>
 <aui:input name="ssn" type="number" value="<%=ssn %>" />
 <aui:input name="email" type="text" value="<%=email %>"/> 
 <aui:input type="submit" value="Update" name="update"></aui:input>
</aui:form>