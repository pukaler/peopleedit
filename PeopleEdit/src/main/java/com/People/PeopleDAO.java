package com.People;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PeopleDAO {

	
	public static String InsertPeople(People p)
	{
		String insStatus="";
		Connection con;
		CallableStatement stmt;
		String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
		
		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con=DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
			
					
			stmt=con.prepareCall("{Call AddPeopleTable(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			{
				 stmt.setString(1, "");	
				 stmt.setString(2, p.getFName());
				 stmt.setString(3, p.getMName());
			     stmt.setString(4, p.getLName());
			     stmt.setString(5, p.getSuffix());
			     stmt.setString(6, p.getFName()+" "+p.getLName());
			     stmt.setString(7, p.getAlias());
			     stmt.setDate(8,new java.sql.Date((p.getdOB().getTime())));
			     stmt.setString(9,p.getdOBString());
			     stmt.setString(10,p.getAge());
			     stmt.setString(11,p.getGender());
			     stmt.setString(12, p.getRace());
			     stmt.setString(13, p.getDLNLicSt());
			     stmt.setString(14, p.getDLNLicType());
			     stmt.setString(15, p.getDLN());
			     stmt.setString(16, p.getSSN());
			     stmt.setString(17, p.getLEP());
			     stmt.setString(18, p.getIntrptrReq());
			     stmt.setString(19, p.getPrimLang());
			     stmt.setString(20,p.getEmail());
			     stmt.setString(21,p.getPhNo());
			     stmt.setString(22,p.getAltPhNo());
			     stmt.setString(23,p.getAddress().getAddr1());
			     stmt.setString(24,p.getAddress().getAddr2());
			     stmt.setString(25,p.getAddress().getCity());
			     stmt.setString(26,p.getAddress().getState());
			     stmt.setString(27,p.getAddress().getZip());
			     stmt.setString(28,p.getAddress().getCountry());
			     stmt.setString(29,p.getPPO());
			     stmt.setString(30, "");
			     stmt.setString(31,"10122");
			     stmt.setString(32, "New");
			     System.out.println("Query: "+stmt);
			     if(stmt.execute())
						insStatus="Not updated succesfully";
					else
						insStatus="Succesfully inserted";
					stmt.close();
		}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return insStatus;
		
	}
	
	public static List<People> DisplayPeople() throws  ClassNotFoundException
	{
		Connection con;
		Statement stmt;
		ResultSet rs;
		String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
		ArrayList<People> plist=new ArrayList<People>();
		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con=DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
			
			stmt=con.createStatement();
			rs=stmt.executeQuery("  select * \r\n" + 
					"  from People P join PeopleAddr PA\r\n" + 
					"  on P.PplId = PA.PplId join Address A\r\n" + 
					"  on PA.AddrId = A.AddrId ");
		
			while(rs.next())
			{
				People p1=new People();
				Address a1=new Address(rs.getString("Addr1"),rs.getString("Addr2"),rs.getString("City"),rs.getString("State"),rs.getString("Country"),rs.getString("Zip"));
				
				p1.setFullName(rs.getString("FullName"));
				p1.setdOBString(rs.getString("DOBString"));
				p1.setGender(rs.getString("Gender"));
				p1.setPhNo(rs.getString("PhNo"));
				
				p1.setAddress(a1);
				System.out.println("Name: "+rs.getString("FullName"));
				plist.add(p1);
		}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return plist;
		
	}
	
}	