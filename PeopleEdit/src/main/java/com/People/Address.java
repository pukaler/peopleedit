package com.People;

public class Address {

	private int addrId;
	private String addr1;
	private String addr2;
	private String city;
	private String country;
	private String state;
	private String zip;	
			
	
	
			
	
	 
	public Address(String addr1, String addr2, String city,  String state, String country, String zip) {
		super();
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.city = city;
		this.country = country;
		this.state = state;
		this.zip = zip;
	}



	public int getAddrId() {
		return addrId;
	}



	public void setAddrId(int addrId) {
		this.addrId = addrId;
	}



	public String getAddr1() {
		return addr1;
	}



	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}



	public String getAddr2() {
		return addr2;
	}



	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getZip() {
		return zip;
	}



	public void setZip(String zip) {
		this.zip = zip;
	}



	@Override public String toString() { return
	  "Address: "+getAddr1()+", "+getAddr2()+", "+getCity()+", "+getState()+", "
	  +getCountry()+", "+getZip(); }
	
}
