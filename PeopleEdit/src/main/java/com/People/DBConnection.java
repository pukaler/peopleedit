package com.People;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnection {

	public static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(Constants.CLASS);
			dbConnection = DriverManager.getConnection(Constants.URL, Constants.UNAME,Constants.PWD);
		} catch (SQLException e) {
			System.out.print("Connection SQLException------"+e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.print("Connection ClassNotFoundException------"+e.getMessage());
		}
		return dbConnection;
	}
	
	
	public static void main(String ar[])
	{
		
		Connection connection = null; 
		ResultSet rs=null;
		 
		try
		{
		 // connection = DriverManager.getConnection(url, "SQLUser1", "PACCPAAM");
		 connection = DBConnection.getDBConnection();
		 Statement st =connection.createStatement();
		 rs=st.executeQuery("select * from People");
		 
		 while(rs.next())
		 {
			 System.out.print("\n"+rs.getString("FullName"));
			 
		 }
		}
		catch(Exception e)
		{
			System.out.println("");
			
		}
		 
		 
	}
}
