package com.People;

import java.util.Date;

import com.People.*;

public class People {

	//private String PplId;

	private String pPlId;
	private Address address;
	private String sID;
	Date dOB;
	private String dOBString;
	private String fullName;
	private String fName;
	private String mName;
	private String lName;
	private String honorific;
	private String suffix;
	private String gender;
	private String sSN;
	private String email;
	private String dLN;
	private String dLNLicSt;
	private String dLNLicType;
	private String alias;
	private String phNo;
	private String altPhNo;
	private String age;
	private String cntyId;
	private String deceased;
	private String ethnicity;
	private String intrptrReq;
	private String isTriMem;
	private String lastMod;
	private String lEP;
	private String modifiedBy;
	private String pPO;
	private String primLang;
	private String race;
	private String triMemId;
	
	
	public String getpPlId() {
		return pPlId;
	}
	public void setpPlId(String pPlId) {
		this.pPlId = pPlId;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getsID() {
		return sID;
	}
	public void setsID(String sID) {
		this.sID = sID;
	}
	public Date getdOB() {
		return dOB;
	}
	public void setdOB(Date dOB) {
		this.dOB = dOB;
	}
	public String getdOBString() {
		return dOBString;
	}
	public void setdOBString(String dOBString) {
		this.dOBString = dOBString;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFName() {
		return fName;
	}
	public void setFName(String fName) {
		this.fName = fName;
	}
	public String getMName() {
		return mName;
	}
	public void setMName(String mName) {
		this.mName = mName;
	}
	public String getLName() {
		return lName;
	}
	public void setLName(String lName) {
		this.lName = lName;
	}
	public String getHonorific() {
		return honorific;
	}
	public void setHonorific(String honorific) {
		this.honorific = honorific;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSSN() {
		return sSN;
	}
	public void setSSN(String sSN) {
		this.sSN = sSN;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDLN() {
		return dLN;
	}
	public void setDLN(String dLN) {
		this.dLN = dLN;
	}
	public String getDLNLicSt() {
		return dLNLicSt;
	}
	public void setDLNLicSt(String dLNLicSt) {
		this.dLNLicSt = dLNLicSt;
	}
	public String getDLNLicType() {
		return dLNLicType;
	}
	public void setDLNLicType(String dLNLicType) {
		this.dLNLicType = dLNLicType;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getPhNo() {
		return phNo;
	}
	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}
	public String getAltPhNo() {
		return altPhNo;
	}
	public void setAltPhNo(String altPhNo) {
		this.altPhNo = altPhNo;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCntyId() {
		return cntyId;
	}
	public void setCntyId(String cntyId) {
		this.cntyId = cntyId;
	}
	public String getDeceased() {
		return deceased;
	}
	public void setDeceased(String deceased) {
		this.deceased = deceased;
	}
	public String getEthnicity() {
		return ethnicity;
	}
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}
	public String getIntrptrReq() {
		return intrptrReq;
	}
	public void setIntrptrReq(String intrptrReq) {
		this.intrptrReq = intrptrReq;
	}
	public String getIsTriMem() {
		return isTriMem;
	}
	public void setIsTriMem(String isTriMem) {
		this.isTriMem = isTriMem;
	}
	public String getLastMod() {
		return lastMod;
	}
	public void setLastMod(String lastMod) {
		this.lastMod = lastMod;
	}
	public String getLEP() {
		return lEP;
	}
	public void setLEP(String lEP) {
		this.lEP = lEP;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getPPO() {
		return pPO;
	}
	public void setPPO(String pPO) {
		this.pPO = pPO;
	}
	public String getPrimLang() {
		return primLang;
	}
	public void setPrimLang(String primLang) {
		this.primLang = primLang;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public String getTriMemId() {
		return triMemId;
	}
	public void setTriMemId(String triMemId) {
		this.triMemId = triMemId;
	}

	
	@Override
	public String toString() {
		return "People2 [pPlId=" + pPlId + ", address=" + address + ", sID=" + sID + ", dOB=" + dOB + ", dOBString="
				+ dOBString + ", fullName=" + fullName + ", fName=" + fName + ", mName=" + mName + ", lName=" + lName
				+ ", honorific=" + honorific + ", suffix=" + suffix + ", gender=" + gender + ", sSN=" + sSN + ", email="
				+ email + ", dLN=" + dLN + ", dLNLicSt=" + dLNLicSt + ", dLNLicType=" + dLNLicType + ", alias=" + alias
				+ ", phNo=" + phNo + ", altPhNo=" + altPhNo + ", age=" + age + ", cntyId=" + cntyId + ", deceased="
				+ deceased + ", ethnicity=" + ethnicity + ", intrptrReq=" + intrptrReq + ", isTriMem=" + isTriMem
				+ ", lastMod=" + lastMod + ", lEP=" + lEP + ", modifiedBy=" + modifiedBy + ", pPO=" + pPO
				+ ", primLang=" + primLang + ", race=" + race + ", triMemId=" + triMemId + "]"  + address.toString();
	}
	
	
}
