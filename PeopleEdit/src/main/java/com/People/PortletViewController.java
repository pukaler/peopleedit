/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.People;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;

import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.util.ReleaseInfo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("VIEW")
public class PortletViewController {

	@RenderMapping
	public String question(Model model) {
		model.addAttribute("releaseInfo", ReleaseInfo.getReleaseInfo());

		return "PeopleEdit/view";
	}
	
	@ActionMapping(value="displaySearchPeople")
	public void submitName(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException 
	{
		String name=ParamUtil.getString(resourceRequest, "name");
		
		System.out.print("name: "+ name);
		name=name+"12334";
		JSONObject jsonResponse = (JSONObject) JSONFactoryUtil.createJSONObject();
		jsonResponse.put("result", name);
	
		PrintWriter writer = resourceResponse.getWriter();
		writer.println(jsonResponse);
	}
	
	
	@ActionMapping(value ="insertPeople")
	public void performInsertUpdate(ActionRequest actReq, ActionResponse actRes, Model model) throws SQLException {
		People p1=new People();
		SimpleDateFormat  df=new SimpleDateFormat ("yyyy-MM-dd");
		Date d2=ParamUtil.getDate(actReq, "dOB", df);
		System.out.print("Date: "+d2.getTime());
		System.out.println("Date in String: "+df.format(d2));
		
		Calendar c = Calendar.getInstance();
		c.setTime(d2);
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH) + 1;
	    int date = c.get(Calendar.DATE);
		LocalDate DOB=LocalDate.of(year, month, date);
		LocalDate now1=LocalDate.now();
		Period diff1 = Period.between(DOB, now1);
		int age=diff1.getYears();
		System.out.println("Age "+age);
		
		p1.setHonorific(ParamUtil.getString(actReq, "honorific"));
		p1.setSuffix(ParamUtil.getString(actReq, "suffix"));
		p1.setAlias(ParamUtil.getString(actReq, "alias"));
		p1.setFName(ParamUtil.getString(actReq, "fName"));
		p1.setMName(ParamUtil.getString(actReq, "mName"));
		p1.setLName(ParamUtil.getString(actReq, "lName"));
		p1.setdOB(d2);
		p1.setdOBString(df.format(d2));
		p1.setAge(String.valueOf(age));
		p1.setDLN(ParamUtil.getString(actReq, "dLN"));
		p1.setDLNLicSt(ParamUtil.getString(actReq, "dLNLicSt"));
		p1.setDLNLicType(ParamUtil.getString(actReq,"dLNLicType"));
		p1.setGender(ParamUtil.getString(actReq,"gender"));
		p1.setEmail(ParamUtil.getString(actReq,"email"));
		p1.setPhNo(ParamUtil.getString(actReq, "phNo"));
		p1.setAltPhNo(ParamUtil.getString(actReq, "altPhNo"));
		p1.setDeceased(ParamUtil.getString(actReq,"deceased"));
		p1.setEthnicity(ParamUtil.getString(actReq,"ethnicity"));
		p1.setRace(ParamUtil.getString(actReq,"race"));
		p1.setLEP(ParamUtil.getString(actReq,"lEP"));
		p1.setPPO(ParamUtil.getString(actReq,"pPO"));
		p1.setPrimLang(ParamUtil.getString(actReq,"primLang"));
		p1.setIntrptrReq(ParamUtil.getString(actReq,"intrptrReq"));
		
		String addr1,addr2,city,state,country,zip;
		addr1=ParamUtil.getString(actReq,"addr1");
		addr2=ParamUtil.getString(actReq,"addr2");
		city=ParamUtil.getString(actReq,"city");
		state=ParamUtil.getString(actReq,"state");
		country=ParamUtil.getString(actReq,"country");
		zip=ParamUtil.getString(actReq,"zipcode");
		Address a1=new Address(addr1,addr2,city,state,country,zip);
		System.out.println("Addr1"+a1.toString());
		p1.setAddress(a1);
		String msg=PeopleDAO.InsertPeople(p1);
		System.out.println("Insert status: "+msg);
		
		System.out.println("People obj: "+p1.getSuffix()+"\t"+p1.getHonorific()+"\t"+p1.getAlias()+"\t"+p1.getFName()+"\t"+p1.getMName()+"\t"+p1.getLName()+"\t"+p1.getEmail()+"\t"+p1.getPhNo()+"\t"+p1.getAltPhNo());
		System.out.println(p1.getDLNLicType()+"\t"+p1.getDLN()+"\t"+p1.getDLNLicSt()+"\t"+p1.getDeceased());
		System.out.println(p1.getPrimLang()+"\t"+p1.getIntrptrReq()+"\t"+p1.getPPO()+"\t"+p1.getLEP()+"\t"+p1.getRace()+"\t"+p1.getEthnicity());
		//System.out.println("input value "+ ParamUtil.getString(actReq, "fName")+"\n"+ParamUtil.getString(actReq,"gender"));
		//System.out.println(ParamUtil.getString(actReq, "mName")+"\n"+ParamUtil.getString(actReq, "lName"));
		//System.out.println(" "+ ParamUtil.getString(actReq,"email")+"\n"+ParamUtil.getString(actReq, "phNo")+"\n"+ParamUtil.getString(actReq,"dLNLicType"));
		model.addAttribute("ProcPeople", p1);
		actRes.setRenderParameter("action",
				    "HomePage");
	}
	
	@RenderMapping(params="action=HomePage")
	public String showSuccess(RenderRequest request, RenderResponse response, Model model)
	{
		return "PeopleEdit/view";
		
	}
	
	@RenderMapping(params = "action=addPeople")
	public String addPeopleURL(RenderRequest request, RenderResponse response, Model model){
	  return "PeopleEdit/InsUpdForm";
	 }
	
	@RenderMapping(params = "action=displayForm")
	public String DisplayRenderURL(RenderRequest request, RenderResponse response, Model model){
		
		System.out.println("Displaying all people");
	  return "PeopleEdit/display_people";
	 }
	
	@ResourceMapping(value="GetPeopleList")
	public void getPeoplelist(ResourceRequest request, ResourceResponse response) throws IOException
	{
		try
		{
			ArrayList<People> plist=new ArrayList<People>();
			System.out.print("In resource URL ()");;
			plist=(ArrayList<People>) PeopleDAO.DisplayPeople();
			String jsonres=JSONFactoryUtil.looseSerializeDeep(plist);
			response.setContentType("application/json");
			 if(jsonres != null && !"".equals(jsonres)) {
				 response.getWriter().print("{\"data\":" + jsonres + "}");
			     } 
			 
			 System.out.println("return from getPeoplelist()");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@RenderMapping(params = "action=updatePeople")
	public String updatePeopleURL(RenderRequest request, RenderResponse response, Model model){
	  return "PeoplePortlet/update_people";
	 }
	
	@RenderMapping(params = "action=searchPeople")
	public String searchPeopleURL(RenderRequest request, RenderResponse response, Model model){
	  return "PeoplePortlet/display_search_people";
	 }
	
	@RenderMapping(params = "action=searchPeopleName")
	public String searchPeopleNameURL(RenderRequest request, RenderResponse response, Model model){
	  return "PeoplePortlet/search_by_name";
	 }
	
	@RenderMapping(params = "action=updatePeopleID")
	public String updatePeopleIDURL(RenderRequest request, RenderResponse response, Model model){
	  return "PeoplePortlet/update_by_id";
	 }
	
	/*public void getPeople(ActionRequest actionRequest,
			   ActionResponse actionResponse) throws IOException,PortalException, SystemException, SQLException {
			  long pPlId = ParamUtil.getLong(actionRequest, "pPlId");
			  System.out.println(pPlId);
			  String cmd = ParamUtil.getString(actionRequest, "cmd");
			  People people = PeopleSearchDao2.getPeopleId(pPlId);
			  actionResponse.setRenderParameter("mvcPath",
			    "nextgen-PeopleUpdate/cases/update_people.jsp");
			 }*/
	
	/*@ActionMapping(value = "updatePeople")
	public void performUpdate(ActionRequest actReq, ActionResponse actRes)
			throws ClassNotFoundException, SQLException {
		People p1 = new People();		
		long pPlId=ParamUtil.getLong(actReq, "pPlId");
		System.out.println("update");
		  
		 p1 = PeopleSearchDao2.getPeopleId(pPlId);
		  if (p1 != null) {
		//p1 = PeopleSearchDao2.getPeopleById(ParamUtil.getInteger(actReq, "pplId"));
		// SimpleDateFormat df=new SimpleDateFormat ("mm-dd-yyyy"); //Date d1=new

		// SimpleDateFormat("dd-mm-yyyy").format(ParamUtil.getDate(actReq, "dOB", df));
		p1.setHonorific(ParamUtil.getString(actReq, "honorific"));
		p1.setSuffix(ParamUtil.getString(actReq, "suffix"));
		p1.setAlias(ParamUtil.getString(actReq, "alias"));
		p1.setFName(ParamUtil.getString(actReq, "fName"));
		p1.setMName(ParamUtil.getString(actReq, "mName"));
		p1.setLName(ParamUtil.getString(actReq, "lName"));
		// p1.setdOB(ParamUtil.getDate(actReq, "dOB",df));
		p1.setDLN(ParamUtil.getString(actReq, "dLN"));
		p1.setDLNLicSt(ParamUtil.getString(actReq, "dLNLicSt"));
		p1.setDLNLicType(ParamUtil.getString(actReq, "dLNLicType"));
		p1.setGender(ParamUtil.getString(actReq, "gender"));
		p1.setEmail(ParamUtil.getString(actReq, "email"));
		p1.setPhNo(ParamUtil.getString(actReq, "phNo"));
		p1.setAltPhNo(ParamUtil.getString(actReq, "altPhNo"));
		p1.setDeceased(ParamUtil.getString(actReq, "deceased"));
		p1.setEthnicity(ParamUtil.getString(actReq, "ethnicity"));
		p1.setRace(ParamUtil.getString(actReq, "race"));
		p1.setLEP(ParamUtil.getString(actReq, "lEP"));
		p1.setPPO(ParamUtil.getString(actReq, "pPO"));
		p1.setPrimLang(ParamUtil.getString(actReq, "primLang"));
		p1.setIntrptrReq(ParamUtil.getString(actReq, "intrptrReq"));

		String addr1, addr2, city, state, country, zip;
		addr1 = ParamUtil.getString(actReq, "addr1");
		addr2 = ParamUtil.getString(actReq, "addr2");
		city = ParamUtil.getString(actReq, "city");
		state = ParamUtil.getString(actReq, "state");
		country = ParamUtil.getString(actReq, "country");
		zip = ParamUtil.getString(actReq, "zipcode");

		Address a1 = new Address(addr1, addr2, city, state, country, zip);
		System.out.println("Addr1 " + a1.toString());
		p1.setAddress(a1);
		
		
		 System.out.println("yes");
		 System.out.println("People obj: " + p1.getSuffix() + "\t" + p1.getHonorific() + "\t" + p1.getAlias() + "\t"
					+ p1.getFName() + "\t" + p1.getMName() + "\t" + p1.getLName() + "\t" + p1.getEmail() + "\t"
					+ p1.getPhNo() + "\t" + p1.getAltPhNo());

			System.out.println(p1.getDLNLicType() + "\t" + p1.getDLN() + "\t" + p1.getDLNLicSt() + "\t" + p1.getDeceased());
			System.out.println(p1.getPrimLang() + "\t" + p1.getIntrptrReq() + "\t" + p1.getPPO() + "\t" + p1.getLEP() + "\t"
					+ p1.getRace() + "\t" + p1.getEthnicity());
		  }
		  p1 = PeopleSearchDao2.UpdatePeople(p1);
		        actRes.setRenderParameter("action",
		        		 "nextgen-PeopleUpdate/cases/display_people.jsp");
		 }*/
	
	
}