package com.People;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PeopleSearchDao {
	
	public static List<People> getPeople() throws SQLException, ClassNotFoundException {
		 
		System.out.println("=============================GET  PEOPLES"); 
		//String url="jdbc:sqlserver://DESKTOP-U11B7VU:1433;databaseName=nextgen-testportlet";
		 
		 //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		 
		 Connection connection = null; ResultSet rs=null;
		 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		 String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
		connection = DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
		 //connection = DBConnection.getDBConnection();
		 System.out.println("Connected db"); 
		 List<People> reqList = new ArrayList<People>(); 
		 PreparedStatement preparedStatement = null; 
		 try {
		String HQL = "SELECT FName, LName, DOB, Gender FROM People ";
		 
		 preparedStatement = connection.prepareStatement(HQL);
		
		 rs = preparedStatement.executeQuery(); 
		 // STEP 5: Extract data from result set
		 while (rs.next()) { 
			 
			 People rq = new People();
		 rq.setFName(rs.getString("FName")); 
		 rq.setLName(rs.getString("LName"));
		 rq.setGender(rs.getString("Gender"));
		 // Retrieve by column name 
		 String fName = rs.getString("FName"); 
		 String lName = rs.getString("LName");
		 String dob = rs.getString("DOBString"); 
		 String gender = rs.getString("Gender"); 
		 reqList.add(rq);
		 // Display values System.out.print("FName: " + fName);
		 System.out.print(", LName: " + lName); System.out.print(", DOB: " + dob);
		 System.out.println(", Gender: " + gender); System.out.println("list added" +
		  rq); }
		  
		 		  
		  } catch (Exception e) { System.out.println("People Search Exception----" +
		  e.getMessage()); } finally { rs.close(); if (preparedStatement != null) {
		  preparedStatement.close(); } if (connection != null) { connection.close(); }
		  } return reqList; 
		  
	}
		  		 
	 
	public static People getPeopleById(long PplId) throws SQLException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		String url = "jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
        connection = DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
		People people = new People();
		try {
	//		connection = DBConnection.getDBConnection();
			 String HQL = "Select * From People P Join PeopleAddr PA "+
			 " on P.PplId = PA.PplId join Address A "+
			  " on PA.AddrId = A.AddrId " +
			 " Where P.PplId = "+ PplId;
			//String HQL = "Select * from People P where P.PplId = "+PplId;
	        preparedStatement = connection.prepareStatement(HQL);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				people.setpPlId(rs.getString("PplId"));
				people.setsID(rs.getString("SID"));
				people.setdOB(rs.getDate("DOB"));
				people.setdOBString(rs.getString("DOBString"));
				people.setFullName(rs.getString("FullName"));
				people.setFName(rs.getString("FName"));
				people.setMName(rs.getString("MName"));
				people.setLName(rs.getString("LName"));
				people.setHonorific(rs.getString("Honorific"));
				people.setSuffix(rs.getString("Suffix")); 
				people.setGender(rs.getString("Gender"));
				people.setSSN(rs.getString("SSN"));
				people.setEmail(rs.getString("Email"));
				people.setDLN(rs.getString("DLN"));
				people.setDLNLicSt(rs.getString("DLNLicSt"));
				people.setDLNLicType(rs.getString("DLNLicType"));
				people.setAlias(rs.getString("Alias"));
				people.setPhNo(rs.getString("PhNo"));
				people.setAltPhNo(rs.getString("AltPhNo"));
				people.setAge(rs.getString("Age"));
				people.setCntyId(rs.getString("CntyId"));
				people.setDeceased(rs.getString("Deceased"));
				people.setEthnicity(rs.getString("Ethnicity"));
				people.setIntrptrReq(rs.getString("IntrptrReq")); 
				people.setIsTriMem(rs.getString("IsTriMem")); 
				people.setLastMod(rs.getString("LastMod")); 
				people.setLEP(rs.getString("LEP")); 
				people.setModifiedBy(rs.getString("ModifiedBy")); 
				people.setPPO(rs.getString("PPO")); 
				people.setPrimLang(rs.getString("PrimLang")); 
				people.setRace(rs.getString("Race")); 
				people.setTriMemId(rs.getString("TriMemId")); 
					    		  
			//	String[] address = people.setAddress(a1);
					      //  if(address !=  null) {
					        	
				/*
				 * String adds=rs.getString("Address"); String[] SepAdds=adds.split(",");
				 * Address a1=new
				 * Address(SepAdds[0],SepAdds[1],SepAdds[2],SepAdds[3],SepAdds[4],SepAdds[5]);
				 * people.setAddress(a1);
				 */
                                //System.out.println(a1.toString());
                                //System.out.println(a1.toString());
			
			}
		} catch (Exception e) {
			System.out.println("getPeopleDetailsById exception-------"+e.getMessage());
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		return people;
	 }
	
	public People getPeopleID(long PplId) throws SQLException, ClassNotFoundException {
		
		  System.out.println("=============================GET  PEOPLE"); Connection
		  connection = null; 
		  ResultSet rs=null;
		  String url = "jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
		  connection = DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
		 // connection = DBConnection.getDBConnection();
		  System.out.println("Connected db"); 
		  People people = new People();
		  PreparedStatement preparedStatement = null;
		  
		  try {
			  
			/*
			 * select * from [nextgen-testportlet].dbo.People P Join
			 * [nextgen-testportlet].dbo.PeopleAddr PA on P.PplId = PA.PplId join Address A
			 * on PA.AddrId = A.addressId Where P.PplId = 16;
			 */
			/*
			 * String HQL =
			 * "Select P.Honorific,P.FName, P.MName,P.LName,P.Suffix,P.FullName,P.DOBString,A.Addr1 +', ' +A.Addr2+', '+A.City+', '+A.State+', '+A.Country+', '+A.Zip 'Address' "
			 * +
			 * " From [nextgen-testportlet].dbo.People  P join [nextgen-testportlet].[dbo].[PeopleAddr] Pa "
			 * + " On P.PplId = PA.PplId  join [nextgen-testportlet].dbo.Address A "+
			 * " On A.AddrId = PA.AddrId"+ " WHERE IsNumeric(P.PplId) = "+ PplId;
			 */
		  String HQL = "Select * from People P where P.PplId = "+PplId;
		  preparedStatement = connection.prepareStatement(HQL);
			rs = preparedStatement.executeQuery();
				while (rs.next()) {
					people.setpPlId(rs.getString("PplId"));
					people.setsID(rs.getString("SID"));
					people.setdOB(rs.getDate("DOB"));
					people.setdOBString(rs.getString("DOBString"));
					people.setFullName(rs.getString("FullName"));
					people.setFName(rs.getString("FName"));
					people.setMName(rs.getString("MName"));
					people.setLName(rs.getString("LName"));
					people.setHonorific(rs.getString("Honorific"));
					people.setSuffix(rs.getString("Suffix")); 
					people.setGender(rs.getString("Gender"));
					people.setSSN(rs.getString("SSN"));
					people.setEmail(rs.getString("Email"));
					people.setDLN(rs.getString("DLN"));
					people.setDLNLicSt(rs.getString("DLNLicSt"));
					people.setDLNLicType(rs.getString("DLNLicType"));
					people.setAlias(rs.getString("Alias"));
					people.setPhNo(rs.getString("PhNo"));
					people.setAltPhNo(rs.getString("AltPhNo"));
					people.setAge(rs.getString("Age"));
					people.setCntyId(rs.getString("CntyId"));
					people.setDeceased(rs.getString("Deceased"));
					people.setEthnicity(rs.getString("Ethnicity"));
					people.setIntrptrReq(rs.getString("IntrptrReq")); 
					people.setIsTriMem(rs.getString("IsTriMem")); 
					people.setLastMod(rs.getString("LastMod")); 
					people.setLEP(rs.getString("LEP")); 
					people.setModifiedBy(rs.getString("ModifiedBy")); 
					people.setPPO(rs.getString("PPO")); 
					people.setPrimLang(rs.getString("PrimLang")); 
					people.setRace(rs.getString("Race")); 
					people.setTriMemId(rs.getString("TriMemId")); 
					
				/*
				 * String adds=rs.getString("Address"); String[] SepAdds=adds.split(",");
				 * Address a1=new
				 * Address(SepAdds[0],SepAdds[1],SepAdds[2],SepAdds[3],SepAdds[4],SepAdds[5]);
				 * people.setAddress(a1);
				 */
                      //System.out.println(a1.toString());
                      //System.out.println(a1.toString());
                 
		  System.out.println("Employee updated with firstname="+people.getFName());
		  System.out.println("Employee updated with fullname="+people.getFullName());
	//	 System.out.println("Employee updated with Address="+people.getAddress());
			
			}
		  }catch(SQLException e){
		  
		  e.printStackTrace(); }finally{ try { preparedStatement.close();
		  connection.close(); } catch (SQLException e) { e.printStackTrace(); } }
		  
		  return people;
		  
		  
		  }
		  
	

   public static ArrayList<People> searchRecord(People people) throws SQLException {

	      Connection connection = null;
          Statement stmt = null;
       //   String url = "jdbc:sqlserver://DESKTOP-U11B7VU:1433;databaseName=nextgen-testportlet";
          ArrayList<People> plist=new ArrayList<People>();
          try {
               // connection = DriverManager.getConnection(url, "SQLUser1", "PACCPAAM");
        	  connection = DBConnection.getDBConnection();
                String sql = "select P.PplId,P.FullName,P.Gender,P.DOBString,A.Addr1 +', ' +A.Addr2+', '+A.City+', '+A.State+', '+A.Country+', '+A.Zip 'Address'\r\n" + 
                          "from People  P join PeopleAddr Pa \r\n" + 
                         "on P.PplId = PA.PplId  join Address A\r\n" + 
                         "on A.AddrId = PA.AddrId" +
                         " WHERE FName='"+people.getFName()+"' OR  LName='"+people.getLName()+"' OR Gender='"+people.getGender()+"' or DOBString='"+people.getdOBString()+"'";
                stmt=connection.createStatement();
                System.out.println("Searching for "+people.getLName());
                ResultSet rs = stmt.executeQuery(sql);
                System.out.println("Executed query");
                int i=0;
                while (rs.next()) {
                                    People p1=new People();
                                    p1.setFullName(rs.getString("FullName"));
                                    p1.setdOBString(rs.getString("DOBString"));
                                    p1.setGender(rs.getString("Gender"));
	                                p1.setpPlId(rs.getString("PplId"));
	                         
	                                String adds=rs.getString("Address");
	                                String[] SepAdds=adds.split(",");
	                                Address a1=new Address(SepAdds[0],SepAdds[1],SepAdds[2],SepAdds[3],SepAdds[4],SepAdds[5]);
	                                p1.setAddress(a1);
	                                //System.out.println(a1.toString());
	                                //System.out.println(a1.toString());
	                                plist.add(p1);
	                                i++;
	                                //System.out.println("Full Name: " + rs.getString("FullName")+"\t DOBString: "+rs.getString("DOBString")+"\t Gender: "+rs.getString("Gender")+"\tAddress: "+adds);
	                                                            
	                               }
	                               System.out.println("i = "+i);
	                              } catch (SQLException e) {
	                                             e.printStackTrace();

	                              } finally {
	                            	  try
	                                             {
	                                            	 	stmt.close();
	                                                    connection.close();
	                                             } catch (SQLException e) {
	                                                            e.printStackTrace();
	                                             }
	                              }

	                              System.out.println("Before return");
	                              return plist;
	               }
   
 public static People UpdatePeople(People p) throws SQLException, ClassNotFoundException
 //  public void updateRequestWarrantPeople(Map<String, String[]> p, People p1) throws SQLException {
   {
	 		
	   // String insStatus="";
         String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";       
         CallableStatement stmt = null;         
     //    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");         
         Connection con=DriverManager.getConnection(url,"SQLUser1","SQLUser1");        
         System.out.println("Connected");
         DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
     
         try {
        	 stmt = con.prepareCall("{call UpdatePeopleTable(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
       // 	 stmt.setString(3, peopleMap.get("editWcsdCaseNo")[0]);    
        	 stmt.setString(1, p.getHonorific());	
			 stmt.setString(2, p.getFName());
			 stmt.setString(3, p.getMName());
			 stmt.setString(4, p.getLName());
			 stmt.setString(5, p.getSuffix());
			 stmt.setString(6, p.getFullName());
			 stmt.setString(7, p.getAlias());
			 stmt.setDate(8,java.sql.Date.valueOf("2002-04-04"));
			// stmt.setDate(8, p.getdOB());
			 stmt.setString(9, p.getdOBString());
			 stmt.setString(10, p.getAge());
			 stmt.setString(11, p.getGender());
			 stmt.setString(12, p.getRace());
			 stmt.setString(13, p.getDLNLicSt());
			 stmt.setString(14, p.getDLNLicType());
			 stmt.setString(15, p.getDLN());
			 stmt.setString(16, p.getSSN()); 
			 stmt.setString(17, p.getLEP()); 
			 stmt.setString(18, p.getIntrptrReq());
			 stmt.setString(19, p.getPrimLang()); 
			 stmt.setString(20, p.getEmail()); 
			 stmt.setString(21, p.getPhNo());
			 stmt.setString(22, p.getAltPhNo ());
			 stmt.setString(23, p.getAddress().getAddr1());
			 stmt.setString(24, p.getAddress().getAddr2());
			 stmt.setString(25, p.getAddress().getCity());
			 stmt.setString(26, p.getAddress().getState());
			 stmt.setString(27, p.getAddress().getZip());
			 stmt.setString(28, p.getAddress().getCountry());
			 stmt.setString(29, p.getpPlId());
              System.out.println("Query: "+stmt);
			
              stmt.execute();
  			
  			//read the OUT parameter now
  			String result = stmt.getString(6);
  			
  			System.out.println("Employee Record Save Success::"+result);
					stmt.close();
					
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//return insStatus;
		return p;
   }
		
		
	                      
 public void updateRequestWarrantPeople(Map<String, String[]> peopleMap, People p) throws SQLException {
	  String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";       
	  CallableStatement stmt = null;         
  //    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");         
      Connection con=DriverManager.getConnection(url,"SQLUser1","SQLUser1");        
      System.out.println("Connected");
      DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
  
      try {
     	 stmt = con.prepareCall("{call UpdatePeopleTable(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
     
     	stmt.setString(1, peopleMap.get("Honorific")[0]);
  		stmt.setString(2, peopleMap.get("FName")[0]);
		 stmt.setString(3, peopleMap.get("MName")[0]);
		 stmt.setString(4, peopleMap.get("LName")[0]);
		 stmt.setString(5, peopleMap.get("Suffix")[0]);
		 stmt.setString(6, peopleMap.get("FullName")[0]);
		 stmt.setString(7, peopleMap.get("Alias")[0]);
		 String birthDate = peopleMap.get("dOB")[0];
	        if(birthDate !=  null && !"".equals(birthDate)) {
	        	stmt.setDate(8, new java.sql.Date(sdf.parse(birthDate).getTime()));
	        } else {
	        	stmt.setDate(5, null);
	        }
		// stmt.setDate(8,java.sql.Date.valueOf("2002-04-04")[0]);
		// stmt.setDate(8, p.getdOB());
		 stmt.setString(9, peopleMap.get("dOBString")[0]);
		 stmt.setString(10, peopleMap.get("Age")[0]);
		 stmt.setString(11, peopleMap.get("Gender")[0]);
		 stmt.setString(12, peopleMap.get("Race")[0]);
		 stmt.setString(13, peopleMap.get("DLNLicSt")[0]);
		 stmt.setString(14, peopleMap.get("DLNLicType")[0]);
		 stmt.setString(15, peopleMap.get("DLN")[0]);
		 stmt.setString(16, peopleMap.get("SSN")[0]); 
		 stmt.setString(17, peopleMap.get("LEP")[0]); 
		 stmt.setString(18, peopleMap.get("IntrptrReq")[0]);
		 stmt.setString(19, peopleMap.get("PrimLang")[0]); 
		 stmt.setString(20, peopleMap.get("Email")[0]); 
		 stmt.setString(21, peopleMap.get("PhNo")[0]);
		 stmt.setString(22, peopleMap.get("AltPhNo")[0]);
		 
		 String[] address = peopleMap.get("Address");
	        if(address !=  null) {
	       // 	stmt.setString(23, p.getAddress().getAddr1());
	        	
	        	stmt.setString(23, peopleMap.get("Addr1")[0]);
	        	stmt.setString(24, peopleMap.get("Addr2")[0]);
	        	stmt.setString(25, peopleMap.get("City")[0]);
	        	stmt.setString(26, peopleMap.get("State")[0]);
	        	stmt.setString(27, peopleMap.get("Zip")[0]);
	        	stmt.setString(28, peopleMap.get("Country")[0]);
	        } else {
	        	stmt.setString(7, "");
	        }
	        
			/*
			 * stmt.setString(23, peopleMap.getAddress().getAddr1()); stmt.setString(24,
			 * peopleMap.getAddress().getAddr2()); stmt.setString(25,
			 * peopleMap.getAddress().getCity()); stmt.setString(26,
			 * peopleMap.getAddress().getState()); stmt.setString(27,
			 * peopleMap.getAddress().getZip()); stmt.setString(28,
			 * peopleMap.getAddress().getCountry());
			 */
        
     
        //System.out.println("HQL--------"+HQL);
	} catch (Exception e) {
		System.out.println("updateInvestigationById exception-------"+e.getMessage());
		e.printStackTrace();
	} finally {
		if (stmt != null) {
			stmt.close();
		}
		if (con != null) {
			con.close();
		}
	}
 }
 
 
   
 public static String InsertPeople(People p) throws SQLException
	{
		String insStatus="";
		Connection con;
		CallableStatement stmt;
		 con = DBConnection.getDBConnection();
		//String url="jdbc:sqlserver://ts-ng-bush:61436;databaseName=nextgen-Test";
	//	 con = DriverManager.getConnection("jdbc:sqlserver://DESKTOP-U11B7VU:1433;databaseName=nextgen-testportlet","SQLUser1","PACCPAAM");
		try
		{
		//	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//con=DriverManager.getConnection(url, "SQLUser1", "SQLUser1");
			
					
			stmt=con.prepareCall("{Call AddPeopleTable(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			{
				 stmt.setString(1, "");	
				 stmt.setString(2, p.getFName());
				 stmt.setString(3, p.getMName());
			     stmt.setString(4, p.getLName());
			     stmt.setString(5, p.getSuffix());
			     stmt.setString(6, p.getFName()+" "+p.getLName());
			     stmt.setString(7, p.getAlias());
			     stmt.setDate(8,java.sql.Date.valueOf("2002-04-04"));
			     stmt.setString(9,p.getdOBString());
			     stmt.setString(10,"20");
			     stmt.setString(11,p.getGender());
			     stmt.setString(12, p.getRace());
			     stmt.setString(13, p.getDLNLicSt());
			     stmt.setString(14, p.getDLNLicType());
			     stmt.setString(15, p.getDLN());
			     stmt.setString(16, p.getSSN());
			     stmt.setString(17, p.getLEP());
			     stmt.setString(18, p.getIntrptrReq());
			     stmt.setString(19, p.getPrimLang());
			     stmt.setString(20,p.getEmail());
			     stmt.setString(21,p.getPhNo());
			     stmt.setString(22,p.getAltPhNo());
			     stmt.setString(23,p.getAddress().getAddr1());
			     stmt.setString(24,p.getAddress().getAddr2());
			     stmt.setString(25,p.getAddress().getCity());
			     stmt.setString(26,p.getAddress().getState());
			     stmt.setString(27,p.getAddress().getZip());
			     stmt.setString(28,p.getAddress().getCountry());
			     stmt.setString(29,p.getPPO());
			     stmt.setString(30, "");
			     stmt.setString(31,"10122");
			     stmt.setString(32, "New");
			     System.out.println("Query: "+stmt);
			     if(stmt.execute())
						insStatus="Not updated succesfully";
					else
						insStatus="Succesfully inserted";
					stmt.close();
		}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return insStatus;
		
	}
}
 /*public static void main(String args[]) throws SQLException, ClassNotFoundException {
	PeopleSearchDao2 p = new PeopleSearchDao2();
//	p.getPeopleIds(101);
//	p.getPeopleById(101);
	p.getPeopleID(16);
	
 }
	*/
	
	 
