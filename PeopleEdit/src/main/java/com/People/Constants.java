package com.People;
import com.liferay.portal.kernel.util.PropsUtil;


	public final class Constants {
		
		/**.
	     * default constructor
	     */
	    private Constants() { }
	    
	
	    public static final String CLASS = PropsUtil.get("db.class");
	    public static final String URL = PropsUtil.get("db.url");
	    public static final String UNAME = PropsUtil.get("db.uname");
	    public static final String PWD = PropsUtil.get("db.pwd");

}

